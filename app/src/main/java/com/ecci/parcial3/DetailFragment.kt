package com.ecci.parcial3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.ecci.parcial3.data.RetrofitInstance
import com.ecci.parcial3.data.model.Coins
import com.ecci.parcial3.data.model.Detail
import com.ecci.parcial3.databinding.FragmentDetailBinding
import com.xwray.groupie.GroupieAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailFragment(private val symbol: String?) : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentDetailBinding>(
                inflater, R.layout.fragment_detail, container, false
        )
        binding.detailFragmentRecyclerView.layoutManager = LinearLayoutManager(requireContext())

        val adapter = GroupieAdapter()
        binding.detailFragmentRecyclerView.adapter = adapter
        // Inflate the layout for this fragment
        RetrofitInstance().cryptocurrenciesApi().getDetails(symbol, "1day", "30", "json").enqueue(object :
                Callback<Detail> {
            override fun onResponse(call: Call<Detail>, response: Response<Detail>) {
                if (response.body() != null) {

                    val detail : Detail = response.body()!!
                    adapter.add(DetailItem(detail))
                }  else {
                    if (response.code() == 401) {

                        Toast.makeText(requireContext(), "El servicio no tiene el token", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(requireContext(), "El servicio ha fallado 1", Toast.LENGTH_SHORT).show()
                    }
                    Toast.makeText(requireContext(), "El servicio ha fallado 2", Toast.LENGTH_SHORT).show()
                }
            }
            override fun onFailure(call: Call<Detail>, t: Throwable) {
                Toast.makeText(requireContext(), "El servicio ha fallado 3", Toast.LENGTH_SHORT).show()
            }
        })
        return binding.root
    }
}