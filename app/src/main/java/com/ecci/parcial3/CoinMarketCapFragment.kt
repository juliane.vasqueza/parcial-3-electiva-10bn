package com.ecci.parcial3

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.ecci.parcial3.data.RetrofitInstance
import com.ecci.parcial3.data.model.Coins
import com.ecci.parcial3.databinding.FragmentCoinmarketcapBinding
import com.xwray.groupie.GroupieAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CoinMarketCapFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentCoinmarketcapBinding>(
            inflater, R.layout.fragment_coinmarketcap, container, false
        )

        binding.coinsRecyclerView.layoutManager = LinearLayoutManager(requireContext())

        val adapter = GroupieAdapter()
        binding.coinsRecyclerView.adapter = adapter

        RetrofitInstance().cryptocurrenciesApi().getCoins().enqueue(object :
            Callback<Coins> {
            override fun onResponse(call: Call<Coins>, response: Response<Coins>) {
                if (response.body() != null) {
                    val coins: Coins = response.body()!!
                    for (c in coins.dataList){
                        adapter.add(CoinsItem(c,{ symbol->
                            val detailFragment = DetailFragment(symbol)
                            val transaction = requireActivity().supportFragmentManager.beginTransaction()
                            transaction.replace(id, detailFragment)
                            transaction.commit()
                        }))
                    }
                }  else {
                        if (response.code() == 401) {
                                      Toast.makeText(requireContext(), "El servicio no tiene el token", Toast.LENGTH_SHORT).show()
                              } else {
                                      Toast.makeText(requireContext(), "El servicio ha fallado 1", Toast.LENGTH_SHORT).show()
                    }
                         Toast.makeText(requireContext(), "El servicio ha fallado 2", Toast.LENGTH_SHORT).show()
                        }
            }
            override fun onFailure(call: Call<Coins>, t: Throwable) {
                Toast.makeText(requireContext(), "El servicio ha fallado 3", Toast.LENGTH_SHORT).show()
            }
        })
        return binding.root
    }
}

