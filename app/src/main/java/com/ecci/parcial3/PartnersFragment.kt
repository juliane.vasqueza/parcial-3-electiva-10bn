package com.ecci.parcial3

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.ecci.parcial3.data.model.Data
import com.ecci.parcial3.databinding.FragmentPartnersBinding
import com.xwray.groupie.GroupieAdapter
import kotlinx.android.synthetic.main.item_detail.*


class PartnersFragment : Fragment() {
    val REQUEST_CODE_GALLERY = 1
    val REQUEST_CODE_CAMERA = 2
    val adapter = GroupieAdapter()
    lateinit var selectedPartner: String
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val binding = DataBindingUtil.inflate<FragmentPartnersBinding>(
            inflater, R.layout.fragment_partners, container, false
        )

        binding.partnersRecyclerView.layoutManager = LinearLayoutManager(context)
        adapter.add(PartnersItem("Sebastian Contreras Gómez", "Belén (Boy)", R.drawable.ic_baseline_sebastian, null, null ,"int",this))
        adapter.add(PartnersItem("Julian Esteban Vasquez Arias", "Bogotá", R.drawable.ic_baseline_julian, null, null,"int",this))

        binding.partnersRecyclerView.adapter = adapter

        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        when(requestCode) {
            REQUEST_CODE_GALLERY -> {
                if(resultCode == RESULT_OK) {
                    val uri = intent?.data
                    if (isSebas()){
                        var list: MutableList<PartnersItem> = mutableListOf()
                        list.add(PartnersItem("Sebastian Contreras Gómez", "Belén (Boy)", 0, uri, null,"uri", this))
                        list.add(adapter.getGroup(1) as PartnersItem)
                        adapter.update(list)
                    }else{
                        var list: MutableList<PartnersItem> = mutableListOf()
                        list.add(adapter.getGroup(0) as PartnersItem)
                        list.add(PartnersItem("Julian Esteban Vasquez Arias", "Bogotá", 0, uri, null,"uri", this))
                        adapter.update(list)
                    }
                }
            }
            REQUEST_CODE_CAMERA -> {
                if(resultCode == RESULT_OK) {
                    val bitmap = intent?.extras?.get("data") as Bitmap
                    if (isSebas()){
                        var list: MutableList<PartnersItem> = mutableListOf()
                        list.add(PartnersItem("Sebastian Contreras Gómez", "Belén (Boy)", 0, null, bitmap,"bitmap", this))
                        list.add(adapter.getGroup(1) as PartnersItem)
                        adapter.update(list)

                    }else{
                        var list: MutableList<PartnersItem> = mutableListOf()
                        list.add(adapter.getGroup(0) as PartnersItem)
                        list.add(PartnersItem("Julian Esteban Vasquez Arias", "Bogotá", 0, null, bitmap,"bitmap", this))
                        adapter.update(list)
                    }

                }
            }
        }
    }

    fun pickerFromGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, REQUEST_CODE_GALLERY)
    }

    fun selectFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, REQUEST_CODE_CAMERA)
    }

    fun updateSelectedPartner(partner: String){
        selectedPartner = partner
    }

    private fun  isSebas(): Boolean{
        return selectedPartner.equals("Sebastian Contreras Gómez")
    }

    fun openLoctionMap(){
        val locationFragment = LocationFragment2()
        if (isSebas()){
            locationFragment.setPartnerLocation("SebasLocation")
        }else{
            locationFragment.setPartnerLocation("JulianLocation")
        }
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(id, locationFragment)
        transaction.commit()
    }
}


