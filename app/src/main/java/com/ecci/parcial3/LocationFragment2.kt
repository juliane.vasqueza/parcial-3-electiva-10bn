package com.ecci.parcial3

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.OnSuccessListener

class LocationFragment2 : SupportMapFragment(), OnMapReadyCallback, OnSuccessListener<Location>,
        GoogleMap.OnMapLongClickListener {
    var partnersLocation: String = ""
    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    val requestLocationPermissionCode = 10

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        validatePermission()
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
        getMapAsync(this)
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap) {
        this.map = p0
        this.map.setOnMapLongClickListener(this)
        fusedLocationClient.lastLocation.addOnSuccessListener(this)
    }

    override fun onSuccess(p0: Location) {
        val label: String
        val latitude: Double
        val longitude: Double
        when {
            partnersLocation.equals("SebasLocation") -> {
                label = "Localización de Sebastian"
                latitude = 5.98957
                longitude = -72.9134
            }
            partnersLocation.equals("JulianLocation") -> {
                label = "Localización de Julian"
                latitude = 4.61
                longitude = -74.082
            }
            else -> {
                label = "Posición Actual"
                latitude = p0.latitude
                longitude = p0.longitude

            }
        }
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latitude, longitude), 17f))
        map.addMarker(MarkerOptions().position(LatLng(latitude, longitude)).title(label))
        partnersLocation = ""
    }

    override fun onMapLongClick(p0: LatLng) {
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(p0.latitude, p0.longitude), 17f))
        map.addMarker(MarkerOptions().position(LatLng(p0.latitude, p0.longitude)).title("Destino"))
    }

    //Permission
    private fun validatePermission() {
        if(ContextCompat.checkSelfPermission(
                        requireContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            nextStep()
        } else {
            requestPermission()
        }
    }

    private fun requestPermission() {
        Toast.makeText(requireContext(),"requestPermission", Toast.LENGTH_SHORT).show()
        activity?.let { ActivityCompat.requestPermissions(it, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), requestLocationPermissionCode) }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == requestLocationPermissionCode) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                nextStep()
            } else {
                Toast.makeText(requireContext(), "El permiso de de localización es necesario", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun nextStep() {
        Toast.makeText(requireContext(), "Permiso de localización otorgado", Toast.LENGTH_SHORT).show()
    }

    fun setPartnerLocation(partnerLocation: String) {
        this.partnersLocation = partnerLocation
    }
}