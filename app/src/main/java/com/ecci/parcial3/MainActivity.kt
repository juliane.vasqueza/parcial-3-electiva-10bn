package com.ecci.parcial3


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.ecci.parcial3.databinding.ActivityMainBinding

class MainActivity() : AppCompatActivity() {

    lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)

        val navController = findNavController(R.id.nav_host_fragment)

        //Configuro mi propio toolbar
        setSupportActionBar(binding.toolbar)

        //Que el TabBar se actualice según navego por la aplicación
        appBarConfiguration = AppBarConfiguration(setOf(R.id.homeFragment, R.id.coinMarketCapFragment,R.id.partnersFragment, R.id.locationFragment2), binding.drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        //Aquí configuramos para especificar que este navview va a usar ese navcontroller
        binding.navView.setupWithNavController(navController)
    }

    // Este código es necesario para abrir el menú o mostrarme la flecha back
    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()

    }
}