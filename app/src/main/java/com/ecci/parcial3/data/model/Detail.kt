package com.ecci.parcial3.data.model

import com.google.gson.annotations.SerializedName
import com.xwray.groupie.Group

data class Detail(

    @SerializedName("symbol")
    val symbol: String,

    @SerializedName("name")
    val name: String,

    @SerializedName("exchange")
    val exchange: String,

    @SerializedName("datetime")
    val datetime: String,

    @SerializedName("open")
    val open: String,

    @SerializedName("high")
    val high: String,

    @SerializedName("low")
    val low: String,

    @SerializedName("close")
    val close: String

)

