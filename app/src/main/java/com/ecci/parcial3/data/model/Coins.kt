package com.ecci.parcial3.data.model

import com.google.gson.annotations.SerializedName

data class Coins(
        @SerializedName("data")
        var dataList: MutableList<Data>
)