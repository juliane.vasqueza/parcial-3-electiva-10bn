package com.ecci.parcial3.data.model

import com.google.gson.annotations.SerializedName

data class Data(

    @SerializedName("symbol")
    val symbol: String,

    @SerializedName("currency_base")
    val base: String,

    @SerializedName("currency_quote")
    val quote: String

    )