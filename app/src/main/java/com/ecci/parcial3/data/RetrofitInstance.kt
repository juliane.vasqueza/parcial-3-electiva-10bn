package com.ecci.parcial3.data


import com.ecci.parcial3.data.api.CryptocurrenciesApi
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInstance {
    private fun getInstance(): Retrofit {
        val gson = GsonBuilder().create()
        val retrofit = Retrofit.Builder()
            .baseUrl("https://twelve-data1.p.rapidapi.com/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        return retrofit
    }

    fun cryptocurrenciesApi(): CryptocurrenciesApi {
        return getInstance().create(CryptocurrenciesApi::class.java)
    }
}

