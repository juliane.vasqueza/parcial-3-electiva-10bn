package com.ecci.parcial3.data.api

import com.ecci.parcial3.data.model.Coins

import com.ecci.parcial3.data.model.Detail
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface CryptocurrenciesApi {

    @Headers("x-rapidapi-key: 57df62a64bmsh9198a273fda1a6ep121b1bjsned2a9edb2250")
    @GET("cryptocurrencies?currency_quote=usd&format=json")
    fun getCoins(): Call<Coins>


  @Headers("x-rapidapi-key: 57df62a64bmsh9198a273fda1a6ep121b1bjsned2a9edb2250")
  @GET("quote")
  fun getDetails(@Query("symbol") symbol: String?, @Query("interval")  interval : String?, @Query("outputsize")  outputsize : String?, @Query("format")  format : String?): Call<Detail>

}