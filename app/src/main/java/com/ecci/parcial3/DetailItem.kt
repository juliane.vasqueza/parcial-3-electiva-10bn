package com.ecci.parcial3

import com.ecci.parcial3.data.model.Data
import com.ecci.parcial3.data.model.Detail
import com.ecci.parcial3.databinding.ItemCoinsBinding
import com.ecci.parcial3.databinding.ItemDetailBinding
import com.xwray.groupie.databinding.BindableItem

class DetailItem(private val detail: Detail): BindableItem<ItemDetailBinding>() {

    override fun bind(viewBinding: ItemDetailBinding, position: Int) {
        viewBinding.symbolTextView.text = detail.symbol
        viewBinding.name.text = "Name: "+detail.name
        viewBinding.exchange.text = "Exchange: "+detail.exchange
        viewBinding.dateTime.text = "DateTime : "+detail.datetime
        viewBinding.open.text = "Open: "+detail.open
        viewBinding.high.text = "High: "+detail.high
        viewBinding.low.text = "Low: "+detail.low
        viewBinding.close.text = "Close: "+detail.close

    }
    override fun getLayout(): Int {
        return R.layout.item_detail
    }


}