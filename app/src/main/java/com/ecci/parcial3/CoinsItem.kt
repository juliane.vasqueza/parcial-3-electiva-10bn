package com.ecci.parcial3

import com.ecci.parcial3.data.model.Data
import com.ecci.parcial3.databinding.ItemCoinsBinding
import com.xwray.groupie.databinding.BindableItem
import javax.security.auth.callback.Callback


class CoinsItem(private val coin: Data, val callback: (String) -> Unit): BindableItem<ItemCoinsBinding>() {

    override fun bind(viewBinding: ItemCoinsBinding, position: Int) {
        viewBinding.symbolTextView.text = coin.symbol
        viewBinding.currencyBaseTextView.text = coin.base
        viewBinding.currencyQuoteTextView.text = coin.quote
        viewBinding.ButtonDetail.setOnClickListener {
            callback(coin.symbol)
        }
    }
    override fun getLayout(): Int {
        return R.layout.item_coins
    }
}