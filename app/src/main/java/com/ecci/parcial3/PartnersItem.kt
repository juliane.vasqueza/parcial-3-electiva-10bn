package com.ecci.parcial3

import android.graphics.Bitmap
import android.net.Uri

import com.ecci.parcial3.databinding.ItemPartnersBinding
import com.xwray.groupie.databinding.BindableItem


class PartnersItem(val name: String, val location: String, val intImage: Int?, val uriImage: Uri?, val bitmapImage: Bitmap?, val image: String, val fragment: PartnersFragment?): BindableItem<ItemPartnersBinding>() {
    override fun bind(viewBinding: ItemPartnersBinding, position: Int) {
        viewBinding.nameTextView.text = name
        viewBinding.locationTextView.text = location
       if (image.equals("int")){
           intImage?.let { viewBinding.imageView.setImageResource(it) }
       }else if(image.equals("uri")){
           viewBinding.imageView.setImageURI(uriImage)
       }else{
           viewBinding.imageView.setImageBitmap(bitmapImage)
       }

        viewBinding.chanceImageViewGallery.setOnClickListener {
            fragment?.updateSelectedPartner(viewBinding.nameTextView.text.toString())
            fragment?.pickerFromGallery()
        }
        viewBinding.chanceImageViewCamera.setOnClickListener {
            fragment?.updateSelectedPartner(viewBinding.nameTextView.text.toString())
            fragment?.selectFromCamera()
        }
        viewBinding.locationImageView.setOnClickListener {
            fragment?.updateSelectedPartner(viewBinding.nameTextView.text.toString())
            fragment?.openLoctionMap()
        }
    }

    override fun getLayout(): Int {
        return R.layout.item_partners
    }
}